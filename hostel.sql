-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 17, 2017 at 01:27 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hostel`
--

-- --------------------------------------------------------

--
-- Table structure for table `dallybazar`
--

CREATE TABLE `dallybazar` (
  `id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `person` varchar(255) NOT NULL,
  `description` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dallybazar`
--

INSERT INTO `dallybazar` (`id`, `date`, `amount`, `person`, `description`) VALUES
(31, 'October 17, 2017', 500, 'fahim ahmed', 'SASA fff');

-- --------------------------------------------------------

--
-- Table structure for table `fund`
--

CREATE TABLE `fund` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `note` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund`
--

INSERT INTO `fund` (`id`, `user_id`, `date`, `amount`, `note`) VALUES
(15, 23, 'October 17, 2017', 222, 'DDD'),
(16, 23, '', 500, ''),
(17, 38, '', 200, ''),
(18, 38, '', 200, ''),
(19, 38, 'November 13, 2017', 2000, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `meal`
--

CREATE TABLE `meal` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `meal` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meal`
--

INSERT INTO `meal` (`id`, `user_id`, `meal`, `date`) VALUES
(463, 23, '2', '8-11-2017'),
(464, 38, '2', '8-11-2017'),
(465, 39, '2', '8-11-2017'),
(470, 40, '10', '8-11-2017'),
(471, 23, '2', '9-11-2017'),
(472, 38, '2', '9-11-2017'),
(473, 39, '2', '9-11-2017'),
(474, 38, '2', '9-11-2017'),
(475, 23, '20', '12-11-2017'),
(477, 39, '20', '12-11-2017'),
(478, 40, '20', '12-11-2017'),
(479, 23, '10', '13-11-2017'),
(480, 38, '1', '13-11-2017'),
(481, 39, '1', '13-11-2017'),
(482, 40, '4', '13-11-2017'),
(483, 23, '10', '18-11-2017'),
(484, 38, '10', '18-11-2017'),
(485, 39, '10', '18-11-2017'),
(486, 40, '10', '18-11-2017');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `alter_mobile` varchar(50) NOT NULL,
  `occupation` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `nid` varchar(255) NOT NULL,
  `room_number` varchar(11) NOT NULL,
  `address` text NOT NULL,
  `about` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `first_name`, `last_name`, `mobile`, `alter_mobile`, `occupation`, `facebook_id`, `institute`, `nid`, `room_number`, `address`, `about`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 23, 'admin', 'admin Khan', '', '', 'Mastari', '', '', '', '03', '', '', '', '2017-10-16 18:38:42', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(21, 38, '', '', '01716240272', '', '', '', '', '', '302', '', 'I ma Fahim ahmed ', '', '2017-11-08 18:00:58', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(22, 39, '', '', '01717 000 000', '', '', '', '', '', '00', '', '', '', '2017-11-08 18:03:46', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(23, 40, '', '', '01717 000 000', '', '', '', '', '', '00', '', '', '', '2017-11-08 18:08:37', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_manager`
--

CREATE TABLE `tbl_manager` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_manager`
--

INSERT INTO `tbl_manager` (`id`, `name`, `password`) VALUES
(1, 'admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(256) NOT NULL,
  `mobile` int(11) NOT NULL,
  `password` varchar(256) NOT NULL,
  `is_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `name`, `email`, `mobile`, `password`, `is_admin`) VALUES
(23, 'admin', 'admin@gmial.com', 1716240250, '123456', 1),
(38, 'Fahim Ahmed', 'fahim52510@gmail.com', 1716240272, '123456', 0),
(39, 'wali ullah', 'waliullha@gmail.com', 1717359439, '123456', 0),
(40, 'Habib Rahman', 'habibrahman@gmail.com', 1717258963, '123456', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dallybazar`
--
ALTER TABLE `dallybazar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fund`
--
ALTER TABLE `fund`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `meal`
--
ALTER TABLE `meal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tbl_manager`
--
ALTER TABLE `tbl_manager`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dallybazar`
--
ALTER TABLE `dallybazar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `fund`
--
ALTER TABLE `fund`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `meal`
--
ALTER TABLE `meal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=487;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `tbl_manager`
--
ALTER TABLE `tbl_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `fund`
--
ALTER TABLE `fund`
  ADD CONSTRAINT `fund_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `meal`
--
ALTER TABLE `meal`
  ADD CONSTRAINT `meal_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tbl_users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
