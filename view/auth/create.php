<?php
include_once 'header.php';
?>
<body class="signup-page">
<div class="signup-box">
    <div class="logo">
        <a href="javascript:void(0);">Signup </a>
        <small>Hostel Management System</small>
    </div>
    <div class="card">
        <div class="body">
            <form action="view/auth/store.php" id="sign_up" method="POST">
                <div class="msg">Register a new membership</div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                    <div class="form-line">
                        <input type="text" class="form-control" name="name" placeholder="Name Surname" required autofocus>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                    <div class="form-line">
                        <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">phone</i>
                        </span>
                    <div class="form-line">
                        <input type="tel" class="form-control" name="mobile" placeholder="Mobile No" required>
                    </div>
                </div>

                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="password" minlength="6" placeholder="Password" required>
                    </div>
                </div>
                <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                    <div class="form-line">
                        <input type="password" class="form-control" name="confirm" minlength="6" placeholder="Confirm Password" required>
                    </div>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                    <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                </div>

                <button class="btn btn-block btn-lg bg-pink waves-effect" name="sign-up" value="signUp" type="submit">SIGN UP</button>

                <div class="m-t-25 m-b--5 align-center">
                    <a href="view/auth/login.php">You already have a membership?</a>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="assets/admin/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="assets/admin/plugins/node-waves/waves.js"></script>

<!-- Validation Plugin Js -->
<script src="assets/admin/plugins/jquery-validation/jquery.validate.js"></script>

<!-- Custom Js -->
<script src="assets/admin/js/admin.js"></script>
<script src="assets/admin/js/pages/examples/sign-up.js"></script>
</body>

</html>