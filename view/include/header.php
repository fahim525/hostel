<?php
if(!isset($_SESSION)){
    session_start();
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Advanced Form Elements | Bootstrap Based Admin Template - Material Design</title>
    <base href="http://localhost/hostel/">
    <!-- Favicon-->
    <link rel="icon" href="../../favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="assets/admin/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="assets/admin/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="assets/admin/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Colorpicker Css -->
    <link href="assets/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css" rel="stylesheet" />

    <!-- Dropzone Css -->
    <link href="assets/admin/plugins/dropzone/dropzone.css" rel="stylesheet">

    <!-- Multi Select Css -->
    <link href="assets/admin/plugins/multi-select/css/multi-select.css" rel="stylesheet">

    <!-- Bootstrap Spinner Css -->
    <link href="assets/admin/plugins/jquery-spinner/css/bootstrap-spinner.css" rel="stylesheet">

    <!-- Bootstrap Tagsinput Css -->
    <link href="assets/admin/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">

    <!-- Bootstrap Select Css -->
    <link href="assets/admin/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

    <!-- noUISlider Css -->
    <link href="assets/admin/plugins/nouislider/nouislider.min.css" rel="stylesheet" />

    <!-- JQuery DataTable Css -->
    <link href="assets/admin/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
    <!-- Custom Css -->
    <link href="assets/admin/css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="assets/admin/css/themes/all-themes.css" rel="stylesheet" />

    <style>
        .form-control {
            border-radius: 0 !important;
        }
    </style>

</head>

<body class="theme-red">
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<!-- #END# Overlay For Sidebars -->
<!-- Search Bar -->
<div class="search-bar">
    <div class="search-icon">
        <i class="material-icons">search</i>
    </div>
    <input type="text" placeholder="START TYPING...">
    <div class="close-search">
        <i class="material-icons">close</i>
    </div>
</div>
<!-- #END# Search Bar -->
<!-- Top Bar -->
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false"></a>
            <a href="javascript:void(0);" class="bars" style="display: block;"></a>
            <a class="navbar-brand" href="javaScript:void()">HOSTEL - Management System</a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <!-- Call Search -->
                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i class="material-icons">search</i></a></li>

            </ul>
        </div>
    </div>
</nav>
<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src=" assets/admin/images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Welcome : <?php echo isset($_SESSION['userName']['name'])?$_SESSION['userName']['name']:''?></div>
                <div class="email">Role: <b>Member</b></div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <?php
                    if($_SESSION['userName']['is_admin'] == 1){
                 ?>
                        <li class="active">
                            <a href="view/manager/index.php">
                                <i class="material-icons">map</i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="view/manager/meal/create.php" class="waves-effect waves-block">
                                <i class="material-icons">today</i>
                                <span>Meal</span>
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                                <i class="material-icons">shopping_basket</i>
                                <span>Bazar</span>
                            </a>
                            <ul class="ml-menu" style="display: block;">
                                <li>
                                    <a href="view/manager/bazar/create.php">
                                        <i class="material-icons">add_circle</i>
                                        <span>Bazar entry</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="view/manager/bazar/index.php">
                                        <i class="material-icons">memory</i>
                                        <span>Bazar View</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block toggled">
                                <i class="material-icons">attach_money</i>
                                <span>Fund</span>
                            </a>
                            <ul class="ml-menu" style="display: block;">
                                <li>
                                    <a href="view/manager/fund/create.php" class=" waves-effect waves-block">
                                        <i class="material-icons">add_circle</i>
                                        <span>Fund Entry</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="view/manager/fund/index.php" class=" waves-effect waves-block">
                                        <i class="material-icons">history</i>
                                        <span>Fund View</span>
                                    </a>
                                </li>
                            </ul>
                        </li>

                  <?php
                    }else {
                ?>
                        <li class="active">
                            <a href="view/users/profile.php">
                                <i class="material-icons">map</i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="view/users/meal.php" class="waves-effect waves-block">
                                <span>Meal</span>
                            </a>
                        </li>
                <?php }?>
            </ul>

        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <a href="view/auth/logout.php" class="btn btn-block btn-lg btn-danger waves-effect">LOGOUT <i class="material-icons">input</i></a>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
    <!-- Right Sidebar -->

    <!-- #END# Right Sidebar -->
</section>
