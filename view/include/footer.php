<!-- Jquery Core Js -->
<script src="assets/admin/plugins/jquery/jquery.min.js"></script>

<!-- Bootstrap Core Js -->
<script src="assets/admin/plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="assets/admin/plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="assets/admin/plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="assets/admin/plugins/node-waves/waves.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="assets/admin/plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="assets/admin/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="assets/admin/plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<!-- Custom Js -->
<script src="assets/admin/js/admin.js"></script>
<script src="assets/admin/js/pages/tables/jquery-datatable.js"></script>
<script src="assets/admin/js/pages/forms/advanced-form-elements.js"></script>
<!-- Demo Js -->
<script src="assets/admin/js/demo.js"></script>


<script type="text/javascript">
    $(document).ready(function (){
        alert('Hi');
        $('#customer').change(function () {
            var customerId = $(this).val();
            $.post('view/meal/data.php?p=customer',{customerid:customerId},function (add) {
                $('#date').html(add);
            });
        });
    });
</script>


</body>

</html>