<?php
session_start();
if(!isset($_SESSION["userName"])) {
    echo '<h1>You are not an authorised user</h1>';
    header('Location:login.php');
    die();
}
?>
<?php
include_once '../../vendor/autoload.php';
$obj = new App\Users\Users();
$user = $obj->view($_SESSION['userName']['id']);
var_dump($user);
?>
<?php include_once '../include/header.php';?>
    <section class="content">
        <div class="clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Update Your Profile
                        </h2>
                    </div>
                    <form action="view/users/update.php" method="post">
                        <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="first_name" class="form-control" value="<?= $user['first_name']?>" placeholder="First Name">
                                        <input type="hidden" name="id" class="form-control" value="<?= $user['id']?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="last_name" class="form-control" value="<?= $user['last_name']?>" placeholder="Last Name">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="occupation" class="form-control" value="<?= $user['occupation']?>" placeholder="Occupation">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="mobile" class="form-control" value="<?=$user['mobile']?>" placeholder="Mobile Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="alter_mobile" class="form-control" value="<?=$user['alter_mobile']?>" placeholder="Alter Mobile Number">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="facebook_id" class="form-control" value="<?=$user['facebook_id']?>" placeholder="Facebook ID">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="institute" class="form-control" value="<?=$user['institute']?>" placeholder="Institute">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="nid" class="form-control" value="<?=$user['nid']?>" placeholder="NID No">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input type="text" name="room_number" class="form-control" value="<?=$user['room_number']?>" placeholder="Room Number">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" name="address" class="form-control no-resize" placeholder="Your Permanent Address ..."></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="form-line">
                                        <textarea rows="4" name="about" class="form-control no-resize" placeholder="About Your Self ..."></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                       <div class="row clearfix">
                           <div class="col-md-12">
                               <div class="pull-right">
                                   <button type="reset" class="btn btn-danger waves-effect">
                                       <i class="material-icons">close</i>
                                       <span>Cancel</span>
                                   </button>
                                   <button type="submit" class="btn btn-success waves-effect">
                                       <i class="material-icons">check</i>
                                       <span>Update</span>
                                   </button>
                               </div>
                           </div>
                       </div>

                    </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php include_once '../include/footer.php'; ?>