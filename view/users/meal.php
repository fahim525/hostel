<?php
session_start();
if(!isset($_SESSION["userName"])) {
    echo '<h1>You are not an authorised user</h1>';
    header('Location:../users/login.php');
    die();
}
?>
<?php
include_once '../../vendor/autoload.php';
$meal = new \App\Manager\Meal\Meal();
//var_dump($_SESSION['userName']['id']);
// Todo Database design again
$sql = "SELECT tbl_users.id ,meal,amount FROM `tbl_users` LEFT JOIN `meal` on tbl_users.id = meal.user_id LEFT JOIN `fund` on tbl_users.id = fund.user_id WHERE tbl_users.id = 38";
$meals = $meal->select($sql);

?>
<?php include '../include/header.php';?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                           Today : <?php echo date("F j, Y")?>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                <tr>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <?php
                                        for($i=0; $i<=date('j'); $i++){
                                           echo "<td>".$i."</td>";
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <?php
//                                        $i=0; $i<=date('j'); $i++
                                        foreach($meals as $meal){
                                            echo "<td>".$meal['meal']."</td>";
                                        }
                                        ?>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include '../include/footer.php';?>

