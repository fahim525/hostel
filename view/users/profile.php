<?php
include_once '../../vendor/autoload.php';
$user = new App\Users\Users();

$profile = $user->view($_SESSION['userName']['id']);

$user =  $_SESSION['userName']['id'];
$meal = new \App\Manager\Meal\Meal();
?>
<?php include_once '../include/header.php'; ?>
    <section class="content">
        <div class="container-fluid">
            <div class="container col-md-12">
                <div class="card">
                        <div class="header">
                            <h2>
                                Welcome to Dashboard <small>Here your all information please Update your profile as soon as possible</small>
                            </h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box-4 hover-zoom-effect">
                                        <div class="icon">
                                            <i class="material-icons col-pink">email</i>
                                        </div>
                                        <div class="content">
                                            <div class="text">Paid Amount</div>
                                            <div class="number">
                                                <?php
                                                $amount = $meal->single_view("SELECT sum(amount) amount FROM `fund` WHERE user_id = $user");
                                                if(isset($amount['amount'])){
                                                    echo $amount['amount'];
                                                }else {
                                                    echo '00';
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box-4 hover-zoom-effect">
                                        <div class="icon">
                                            <i class="material-icons col-light-blue">access_alarm</i>
                                        </div>
                                        <div class="content">
                                            <div class="text">Total Meal</div>
                                            <div class="number">
                                                <?php
                                                $meals = $meal->single_view("SELECT sum(meal) meal FROM `meal` WHERE user_id = $user");
                                                echo $meals['meal'];
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box-4 hover-zoom-effect">
                                        <div class="icon">
                                            <i class="material-icons col-cyan">gps_fixed</i>
                                        </div>
                                        <div class="content">
                                            <div class="text">Meal Rate</div>
                                            <div class="number">
                                                <?php
                                                $rat = $meal->single_view('SELECT sum(amount) amount FROM `dallybazar`');
                                                echo round($rat['amount'] / $meals['meal'], 2);
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                    <div class="info-box-4 hover-zoom-effect">
                                        <div class="icon">
                                            <i class="material-icons col-blue">devices</i>
                                        </div>
                                        <div class="content">
                                            <div class="text">Remaining Balance</div>
                                            <div class="number">
                                                <?php echo $amount['amount'] - ($rat['amount'] / $meals['meal'])*$meals['meal'] ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="button" class="btn btn-danger waves-effect">
                                        <i class="material-icons">lock</i><span>Meal Off</span>
                                    </button>
                                    <a href="view/users/editProfile.php" class="btn btn-primary waves-effect">
                                        <i class="material-icons">chat</i><span>Contact</span>
                                    </a>
                                    <a href="view/users/edit.php" class="btn btn-primary waves-effect">
                                        <i class="material-icons">settings</i><span>Update Profile</span>
                                    </a>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <!--left col-->
                                    <ul class="list-group">
                                        <li class="list-group-item text-muted" contenteditable="false">Profile</li>
                                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Joined</strong></span><?php echo $profile['created_at']?></li>
                                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Last seen</strong></span> Yesterday</li>
                                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Room no</strong></span><?php echo $profile['room_number']?></li>
                                        <li class="list-group-item text-right"><span class="pull-left"><strong class="">Phone Number: </strong></span> <?php echo $profile['mobile']?></li>
                                    </ul>
                                </div>                                <!--/col-3-->
                                <div class="col-sm-8" style="" contenteditable="false">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">Full Name</div>
                                        <div class="panel-body"><?php echo $profile['first_name'].' '.$profile['last_name'] ?></div>
                                        <div class="panel-heading" contenteditable="false">Occupation</div>
                                        <div class="panel-body"><?php echo $profile['occupation']; ?></div>
                                        <div class="panel-heading" contenteditable="false">Another Mobile Number</div>
                                        <div class="panel-body"><?php echo $profile['alter_mobile']; ?></div>
                                        <div class="panel-heading" contenteditable="false">Permanent Address</div>
                                        <div class="panel-body"><?php echo $profile['address']; ?></div>
                                        <div class="panel-heading">About Your self</div>
                                        <div class="panel-body"><?php echo $profile['about']; ?></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </section>
<?php include_once '../include/footer.php'; ?>