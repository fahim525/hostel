<?php
session_start();
include_once '../../src/manager/Manager.php';
$manager = new Manager();
if(!isset($_SESSION["managerName"])) {
    echo '<h1>You are not an authorised user</h1>';
    header('Location:../users/login.php');
    die();
}
?>
<?php
if (isset($_POST['submit'])){
    $date = $_POST['date'];
    $amount= $_POST['amount'];
    $name= $_POST['name'];
    


    $manager->setDate($date);
    $manager->setAmount($amount);
    $manager->setName($name);
    

    if ($manager->insertFund()){
        $_SESSION['fund']='Fund Successfully added into Database';
        header('Location: fund.php');
    }else {
        echo 'something wrong';
    }

}