<?php
session_start();
include_once '../../vendor/autoload.php';

$manager = new \App\Manager\Manager();
/*
$totalSpent = $manager->totalSpent()[0];
$totalFund = $manager->totalFund()[0];
$totalMeal = $manager->countMeal()[0];
$mealRate = $totalSpent / $totalMeal ;
*/


?>
<?php include_once '../include/header.php';?>

    <section class="content">
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">attach_money</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Spent</div>
                            <div class="number count-to" data-from="0" data-to="10000" data-speed="15" data-fresh-interval="20"><?php echo 25 ; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">opacity</i>
                        </div>
                        <div class="content">
                            <div class="text">Total Fund</div>
                            <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo 85 ; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">storage</i>
                        </div>
                        <div class="content">
                            <div class="text">Total meal</div>
                            <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20"><?php echo 858 ; ?></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect">
                        <div class="icon">
                            <i class="material-icons">pie_chart</i>
                        </div>
                        <div class="content">
                            <div class="text">Meal Rate</div>
                            <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20"><?php echo 855 ; ?></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <?php
                $i = 0;
                $sql = "SELECT * FROM `tbl_users` WHERE is_admin = 0";
                foreach($manager->select($sql) as $user){
                    $i++;
                    ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <div class="header bg-green">
                            <h2>
                                <?php echo $user['name']?> <small><?php echo $user['mobile']?></small>
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">more_vert</i>
                                    </a>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Another action</a></li>
                                        <li><a href="javascript:void(0);" class=" waves-effect waves-block">Something else here</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <div class="body">
                            <ul class="list-group">
                                <li class="list-group-item">Total Meal<span class="badge bg-pink">14</span></li>
                                <li class="list-group-item">Total Paid Amoutn<span class="badge bg-cyan">99</span></li>
                                <li class="list-group-item">Total Bazar Expanse<span class="badge bg-teal">99</span></li>
                                <li class="list-group-item">Last Paid Amount Date<span class="badge bg-orange">21</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php include_once '../include/footer.php'; ?>