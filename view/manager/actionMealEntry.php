<?php
session_start();
include_once '../../src/manager/Meal.php';
$manager = new Meal();
$count = $manager->countUser()[0];
if(!isset($_SESSION["managerName"])) {
    echo '<h1>You are not an authorised user</h1>';
    header('Location:../users/login.php');
    die();
}
?>
<?php
$meal = new Meal();
if(isset($_POST['submit'])) {
    for ( $i=0 ; $i <= $count ; $i++ ){
        $name = $_POST['name'][$i];
        $meal = $_POST['meal'][$i];
        $date= $_POST['date'][$i];
            $manager->setName($name);
            $manager->setMeal($meal);
            $manager->setDate($date);

            $manager->insertMeal();

    }
    $_SESSION['mealEntry'] = 'Meal Successfully entry into Database';
    header('Location: mealEntry.php');
}
?>

