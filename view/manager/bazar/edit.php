<?php
include_once '../../include/header.php';
include_once '../../../vendor/autoload.php';
$manager = new App\Manager\Manager();
$bazar = new App\Manager\Bazar\Bazar();
$bazar = $bazar->view($_GET['id']);
?>

<section class="content">
    <div class="card">
        <div class="header">
            <h4>Update Bazar Information</h4>
            <small>This access only for Admin</small>
        </div>
        <div class="body">
            <form action="view/manager/bazar/update.php" method="post">
                <div class="row clearfix">
                    <div class="col-md-8 col-md-offset-2">
                        <?php if (isset($_SESSION['msg'])){echo $_SESSION['msg']; } ?>
                        <div class="form-group">
                            <label for="date">Date</label>
                            <div class="form-line">
                                <input type="text" name="date" value="<?php echo $bazar['date']?>" class="form-control">
                                <input type="hidden" name="id" value="<?php echo $bazar['id']?>" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Amount</label>
                            <div class="form-line">
                                <input type="number" name="amount" value="<?php echo $bazar['amount']?>" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Bazar Person</label>
                            <div class="form-line">
                                <select name="person" class="form-control show-tick">
                                    <option value="No person selected">Select One</option>
                                    <?php
                                    $sql = "SELECT * FROM `tbl_users`";
                                    $users = $manager->select($sql);
                                    foreach ($users as $user){
                                        ?>
                                        <option <?= ($user['name'] == $bazar['person'])?'selected':''?> value="<?=$user['name']?>"><?=$user['name']?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <textarea name="description" class="form-control" cols="20" rows="5" placeholder="Bazar Description"><?= $bazar['description']?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="pull-right">
                            <button type="reset" class="btn btn-danger waves-effect">
                                <i class="material-icons">close</i>
                                <span>Cancel</span>
                            </button>
                            <button type="submit" class="btn btn-success waves-effect">
                                <i class="material-icons">check</i>
                                <span>Submit</span>
                            </button>
                        </div>
                    </div>
                </div>
                <form>
        </div>
    </div>
</section>
<?php include_once '../../include/footer.php'; ?>
