<?php
session_start();

include_once '../../../vendor/autoload.php';
$bazar = new App\Manager\Bazar\Bazar();
/*$totalSpent = $user->totalSpent()[0];
$totalFund = $user->totalFund()[0];
*/

?>
<?php include '../../include/header.php';?>
<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="card">
                                    <div class="header">
                                        <h2>
                                            All Bazar List
                                        </h2>
                                    </div>
                                    <div class="body">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped table-hover dataTable js-exportable">
                                                <thead>
                                                    <tr>
                                                        <th style="width:20px">No</th>
                                                        <th style="width:120px">Name</th>
                                                        <th style="width:120px">Date</th>
                                                        <th style="width:20px">Amount</th>
                                                        <th style="">Description date</th>
                                                        <th style="width:150px">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $i = 0;
                                                    foreach($bazar->index() as $value){
                                                        $i++;
                                                        ?>
                                                        <tr role="row" class="odd">
                                                            <td class="sorting_1"><?php echo $i; ?></td>
                                                            <td><?php echo $value['person']; ?></td>
                                                            <td><?php echo $value['date']; ?></td>
                                                            <td><?php echo $value['amount']; ?></td>
                                                            <td><?php echo $value['description']; ?></td>
                                                            <td>
                                                                <?php echo "<a href='view/manager/bazar/edit.php?id=".$value['id']."'>Edit</a>" ?> ||
                                                                <?php echo "<a href='view/manager/bazar/delete.php?id=".$value['id']."' onclick='return confirm(\"Are you  want to delete this data??\")'>Delete</a>" ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
</section>

<?php include '../../include/footer.php';?>

