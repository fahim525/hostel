<?php
session_start();
include_once '../../../vendor/autoload.php';
$manager = new App\Manager\Manager();

/*$totalSpent = $manager->totalSpent()[0];
$totalFund = $manager->totalFund()[0];*/

?>
<?php include_once '../../include/header.php';?>

    <section class="content">
       <div class="card">
           <div class="header">
               <h4>Daily Bazar Cost</h4>
               <small>Showing Date is current Date. You can also edit it </small>
           </div>
           <div class="body">
               <form action="view/manager/bazar/store.php" method="post">
                   <div class="row clearfix">
                       <div class="col-md-8 col-md-offset-2">
                           <div class="form-group">
                               <label for="date">Date</label>
                               <div class="form-line">
                                   <input type="text" name="date" value="<?php echo date('F j, Y')?>" class="form-control" id="date">
                               </div>
                           </div>
                           <div class="form-group">
                               <label for="exampleInputPassword1">Amount</label>
                               <div class="form-line">
                                   <input type="number" name="amount" class="form-control" id="exampleInputPassword1" placeholder="Taka">
                               </div>
                           </div>

                           <div class="form-group">
                               <label for="exampleInputPassword1">Bazar Person</label>
                               <div class="form-line">
                                   <select name="person" class="form-control show-tick">
                                       <option value="No person selected">Select One</option>
                                       <?php
                                       $sql = "SELECT * FROM `tbl_users`";
                                       $users = $manager->select($sql);
                                       foreach ($users as $user){
                                           echo "<option value=".$user['name'].">".$user['name']."</option>";
                                           ?>
                                       <?php }?>
                                   </select>
                               </div>
                           </div>
                           <div class="form-group">
                               <textarea name="description" class="form-control" cols="20" rows="5" placeholder="Bazar Description"></textarea>
                           </div>

                       </div>
                   </div>
                   <div class="row clearfix">
                       <div class="col-md-8 col-md-offset-2">
                           <div class="pull-right">
                               <button type="reset" class="btn btn-danger waves-effect">
                                   <i class="material-icons">close</i>
                                   <span>Cancel</span>
                               </button>
                               <button type="submit" class="btn btn-success waves-effect">
                                   <i class="material-icons">check</i>
                                   <span>Submit</span>
                               </button>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
       </div>
    </section>
<?php include_once '../../include/footer.php'; ?>

