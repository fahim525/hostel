<?php

include_once '../../../vendor/autoload.php';
use App\Manager\Meal\Meal;

$meal = new Meal();


$page = isset($_GET['p'])? $_GET['p']: '';

if($page == 'customer'){
    $customeId = $_POST['customerid'];
    $query = 'SELECT `date` FROM `meal` WHERE user_id ='.$customeId;
    $dates = $meal->select($query);
    foreach ($dates as $date){
        ?>
            <option value="<?php echo $date['date'] ?>"><?php echo date('d', strtotime($date['date'])) ?></option>
        <?php
    }
}

