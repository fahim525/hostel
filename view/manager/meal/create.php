<?php
session_start();
include_once '../../../vendor/autoload.php';
$meal = new App\Manager\Meal\Meal();
$user = new App\Manager\Manager();
$disable = $db_user = '';
$sql = "SELECT tbl_users.name, user_id, GROUP_CONCAT(meal) meal, GROUP_CONCAT(date) date FROM `meal` RIGHT JOIN tbl_users on tbl_users.id = meal.user_id GROUP by user_id";
$allmeals = $meal->select($sql); $sl = 1;

?>
<?php include_once '../../include/header.php';?>
    <section class="content">
    <div class="card">
        <div class="header">
            <h4><?= date('l : jS / F / Y')?></h4>
            <small>According to Date meal have to shown .</small>
            <div class="">

                <?php
                    if(isset($_SESSION['error']) || isset($_SESSION['success'])){
                       // echo $_SESSION['error'];
                        session_unset();
                    }
                ?>

            </div>
        </div>
        <div class="body">
            <form action="view/manager/meal/store.php" method="post">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Total</th>
                                    <th>Input Daily meal / <a href="" class="text-danger" data-toggle="modal" data-target="#smallModal">Update</a></th>
                                    <th width="50px">
                                        <table>
                                            <tr>
                                                <td>Date</td>
                                                <?php
                                                foreach ($allmeals as $item){
                                                    $dates = explode(',',$item['date']);
                                                    foreach ($dates as $date){
                                                        echo "<td>".date('d', strtotime($date))."</td>";
                                                    }break;
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                foreach ($allmeals as $item){

                                    $dates = explode(',',$item['date']);
                                    foreach ($dates as $date){
                                        if(date('d', strtotime($date)) == date('d')){
                                            $db_user = $item['user_id'];
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <th><?php echo $sl++ ?></th>
                                        <th><?php echo $item['name'] ?></th>
                                        <td><b><?php echo array_sum(explode(',',$item['meal'])); ?></b></td>
                                        <td>
                                            <div>
                                                <input type="hidden" name="user_id[]" value="<?= $item['user_id']?>">
                                                <input <?php
                                                        echo ($item['user_id']== $db_user )? 'readonly':''
                                                        ?>
                                                        type="text"
                                                        name="meal[]"
                                                        class="form-control"
                                                        placeholder="00" />
                                            </div>
                                        </td>

                                        <td>
                                           <table>
                                               <tr>
                                                   <td><b>Meal</b></td>
                                                   <?php
                                                    $meals = explode(',',$item['meal']);
                                                    foreach ($meals as $meal){
                                                        if($meal>9){
                                                            echo "<td>$meal</td>";
                                                        }else{
                                                            echo "<td>0$meal</td>";
                                                        }

                                                   } ?>
                                               </tr>
                                           </table>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <button type="reset" class="btn btn-danger waves-effect">
                                <i class="material-icons">close</i>
                                <span>Cancel</span>
                            </button>
                            <button type="submit" name="submit" class="btn btn-success waves-effect">
                                <i class="material-icons">check</i>
                                <span>Submit</span>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

        <!-- Modal Dialogs -->
        <!-- Default Size -->
        <div class="modal fade" id="smallModal" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="defaultModalLabel">Modal title <span id="show"></span></h4>
                    </div>

                    <form action="view/manager/meal/update.php" method="post">
                        <div class="modal-body">
                            <table class="table table-bordered">
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="user_id" id="customer" class="form-control show-tick">
                                                    <option value="No person selected">Select One</option>
                                                    <?php
                                                    foreach ($allmeals as $item){
                                                    ?>
                                                    <option value="<?= $item['user_id']?>"><?= $item['name']?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <select name="date" class="form-control show-tick">
                                                    <?php
                                                    for ( $i=1; $i<= date('d'); $i++){
                                                        ?>
                                                        <option value="<?= $i.'-'.date('m-Y') ?>"><?= $i.'-'.date('m-Y')?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="meal" placeholder="00">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <div class="pull-right">
                                <button type="reset" class="btn btn-danger waves-effect" data-dismiss="modal">
                                    <i class="material-icons">close</i>
                                    <span>Cancel</span>
                                </button>
                                <button type="submit" name="submit" class="btn btn-success waves-effect">
                                    <i class="material-icons">check</i>
                                    <span>Update</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

</section>
<?php include_once '../../include/footer.php'; ?>
