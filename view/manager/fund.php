<?php
session_start();
//include_once '../../src/manager/Manager.php';
/*$manager = new Manager();
$totalSpent = $manager->totalSpent()[0];*/
//$totalFund = $manager->totalFund()[0];

?>
<?php include_once '../include/header.php';?>

<section class="content">
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-pink hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">playlist_add_check</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Spent</div>
                        <div class="number count-to" data-from="0" data-to="10000" data-speed="15" data-fresh-interval="20"><?php echo '000' ; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-cyan hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">help</i>
                    </div>
                    <div class="content">
                        <div class="text">Fund</div>
                        <div class="number count-to" data-from="0" data-to="257" data-speed="1000" data-fresh-interval="20"><?php echo '2555' ; ?></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-light-green hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">forum</i>
                    </div>
                    <div class="content">
                        <div class="text">Due</div>
                        <div class="number count-to" data-from="0" data-to="243" data-speed="1000" data-fresh-interval="20">243</div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="info-box bg-orange hover-expand-effect">
                    <div class="icon">
                        <i class="material-icons">person_add</i>
                    </div>
                    <div class="content">
                        <div class="text">Total Cost</div>
                        <div class="number count-to" data-from="0" data-to="1225" data-speed="1000" data-fresh-interval="20">1225</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <form action="view/manager/actionFundEntry.php" method="post">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Date</label>
                            <input type="date" name="date" class="form-control" required="on" >
                        </div>
                        <div class="form-group">
                            <label for="inputCourse" class="col-form-label">Name</label>
                            <input type="text" class="form-control" id="inputCourse" list="course" placeholder="Select from Dropdown List..." name="name" required="on" />
                            <datalist id="course">

                                <option selected="selected">Choose one</option>
                               <!-- <?php
/*                                foreach($manager->readAll() as  $value) { */?>
                                    <option value="<?php /*echo $value['name'] */?>"><?php /*echo $value['name'] */?></option>
                                    --><?php
/*                                } */?>

                            </datalist>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Amount</label>
                            <input type="number" name="amount" class="form-control" id="exampleInputPassword1" placeholder="Input Taka" required="on">
                        </div>

                        <button type="submit" name="submit"  class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include_once '../include/footer.php'; ?>

