<?php
/**
 * Created by PhpStorm.
 * User: Fahim
 * Date: 10/17/2017
 * Time: 2:20 AM
 */
namespace App\Manager\Fund;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDO;
use PDOException;


class Fund extends Connection
{
    protected $date;
    protected $amount;
    protected $note;
    protected $user_id;

    public function set($data = array()){
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('amount',$data)){
            $this->amount = $data['amount'];
        }
        if(array_key_exists('note',$data)){
            $this->note = $data['note'];
        }
        if(array_key_exists('user_id',$data)){
            $this->user_id = $data['user_id'];
        }
        return $this;
    }

    public function store(){
        try{

            $sql = "INSERT INTO `fund` (user_id,date,amount,note) VALUES (:user_id, :date,:amount,:note)";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':user_id'=>$this->user_id,
                ':date'=>$this->date,
                ':amount'=>$this->amount,
                ':note'=>$this->note
            ));
            if($stmt){
                $_SESSION['success'] = 'Record successfully Inserted !!!';
                header('location: create.php');
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function index(){
        try{
            $sql = "SELECT fund.id, `name`,`mobile`, `user_id`,SUM(amount) as amount, `date`, `note` FROM `tbl_users` LEFT JOIN `fund` ON `tbl_users`.id = `fund`.`user_id` GROUP by user_id";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

}