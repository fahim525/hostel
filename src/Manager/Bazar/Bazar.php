<?php
/**
 * Created by PhpStorm.
 * User: Fahim
 * Date: 10/17/2017
 * Time: 2:20 AM
 */
namespace App\Manager\Bazar;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDO;
use PDOException;


class Bazar extends Connection
{
    protected $date;
    protected $amount;
    protected $person;
    protected $description;

    public function set($data = array()){
        if(array_key_exists('date',$data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('amount',$data)){
            $this->amount = $data['amount'];
        }
        if(array_key_exists('person',$data)){
            $this->person = $data['person'];
        }
        if(array_key_exists('description',$data)){
            $this->description = $data['description'];
        }
        return $this;
    }
    public function store(){
        try{

            $sql = "INSERT INTO dallybazar (date,amount,person,description) VALUES (:date,:amount,:person,:description)";
            $stmt = $this->db->prepare($sql);
            $stmt->execute(array(
                ':date'=>$this->date,
                ':amount'=>$this->amount,
                ':person'=>$this->person,
                ':description'=>$this->description
            ));
            if($stmt){
                $_SESSION['success'] = 'Record successfully Inserted !!!';
                header('location: index.php');
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function index(){
        try{
            $sql = "SELECT * FROM dallybazar ORDER by id DESC";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function view($id){
        try{
            $sql = "SELECT * FROM dallybazar WHERE id = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function delete($id){
        try{
            $sql = "DELETE FROM dallybazar WHERE id = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['success'] = 'Record successfully Deleted !!!';
                header('location: index.php');
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function update($id){
        try{
            $sql = "UPDATE `dallybazar` SET `date` = :date, `amount` = :amount, `person` = :person, `description` = :description WHERE `dallybazar`.`id` = :id;";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':date', $this->date, PDO::PARAM_STR);
            $stmt->bindValue(':amount', $this->amount, PDO::PARAM_STR);
            $stmt->bindValue(':person', $this->person, PDO::PARAM_STR);
            $stmt->bindValue(':description', $this->description, PDO::PARAM_STR);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['success'] = 'Record successfully Updated !!!';
                header('location: index.php');
            }

        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}