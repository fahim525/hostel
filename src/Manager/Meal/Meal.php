<?php
/**
 * Created by PhpStorm.
 * User: Fahim
 * Date: 10/17/2017
 * Time: 2:20 AM
 */
namespace App\Manager\Meal;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDO;
use PDOException;


class Meal extends Connection
{
    private $user_id = '';
    private $date = '';
    private $meal = '';

    public function set($data = array()){
        if(array_key_exists('user_id', $data)){
            $this->user_id = $data['user_id'];
        }
        if(array_key_exists('date', $data)){
            $this->date = $data['date'];
        }
        if(array_key_exists('meal', $data)){
            $this->meal = $data['meal'];
        }

        return $this;

    }

    public function store($data){
        $name = '';
        $key = '';

        $stmt = $this->db->prepare("INSERT INTO `meal`(`user_id`, `meal`, `date`) VALUES(:user_id, :meal, :date)");
        $stmt->bindParam(':user_id', $key, PDO::PARAM_STR);
        $stmt->bindParam(':meal', $name, PDO::PARAM_STR);
        $stmt->bindParam(':date', ltrim(date('d-m-Y'), '0'), PDO::PARAM_STR);
        foreach($data as $key => $name) {
            $stmt->execute();
        }
        if($stmt){
            $stmt = $this->db->prepare("DELETE FROM `meal` WHERE meal = ''");
            $stmt->execute();
            $_SESSION['success'] = 'Meal information successfully Inserted !!!';
            header('location: create.php');
        }
    }

    public function select($sql){
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function single_view($sql){
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }



    public function update(){
        try{

            $sql = "UPDATE `meal` SET `meal` = :meal WHERE `user_id` = :user_id AND `date` = :date";
            $stmt = $this->db->prepare($sql);
            $stmt->bindParam(':meal', $this->meal, PDO::PARAM_INT);
            $stmt->bindParam(':user_id', $this->user_id, PDO::PARAM_STR);
            $stmt->bindParam(':date', $this->date, PDO::PARAM_STR);
            $stmt->execute();
            if($stmt){
                $_SESSION['success'] = 'Successfully Updated !!!';
                header('location: create.php');
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}