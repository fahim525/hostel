<?php
namespace App\Manager;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDO;
use PDOException;
class Manager extends Connection
{
    public function select($sql){
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

}