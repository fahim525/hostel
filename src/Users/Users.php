<?php
namespace App\Users;
if(!isset($_SESSION)){
    session_start();
}
use App\Database\Connection;
use PDO;
use PDOException;
class Users extends Connection
{
    protected $name;
    protected $first_name;
    protected $last_name;
    protected $email;
    protected $password;
    protected $mobile;
    protected $alter_mobile;
    protected $occupation;
    protected $facebook_id;
    protected $institute;
    protected $nid;
    protected $room_number;
    protected $address;
    protected $about;
    protected $image;

    public function set($data = array ()){
        if(array_key_exists('name',$data)){
            $this->name = $data['name'];
        }
        if(array_key_exists('first_name',$data)){
            $this->first_name = $data['first_name'];
        }
        if(array_key_exists('last_name',$data)){
            $this->last_name = $data['last_name'];
        }
        if(array_key_exists('occupation',$data)){
            $this->occupation = $data['occupation'];
        }
        if(array_key_exists('institute',$data)){
            $this->institute = $data['institute'];
        }

        if(array_key_exists('facebook_id',$data)){
            $this->facebook_id = $data['facebook_id'];
        }
        if(array_key_exists('nid',$data)){
            $this->nid = $data['nid'];
        }
        if(array_key_exists('room_number',$data)){
            $this->room_number = $data['room_number'];
        }
        if(array_key_exists('address',$data)){
            $this->address = $data['address'];
        }
        if(array_key_exists('about',$data)){
            $this->about = $data['about'];
        }
        if(array_key_exists('email',$data)){
            $this->email = $data['email'];
        }
        if(array_key_exists('mobile',$data)){
            $this->mobile = $data['mobile'];
        }
        if(array_key_exists('alter_mobile',$data)){
            $this->alter_mobile = $data['alter_mobile'];
        }
        if(array_key_exists('password',$data)){
            $this->password = $data['password'];
        }
        if(array_key_exists('confirm',$data)){
            $this->confirm = $data['confirm'];
        }
        if(array_key_exists('terms',$data)){
            $this->terms = $data['terms'];
        }
        return $this;
    }


    public function store(){
        try {
        $sql = "INSERT INTO tbl_users (name,email,mobile,password) VALUES (:name,:email,:mobile,:password)";
        $stmt = $this->db->prepare($sql);
        $stmt->execute(array(
            ':name' => $this->name,
            ':email' => $this->email,
            ':mobile' => $this->mobile,
            ':password' => $this->password
        ));
        if($stmt){
            $stmt = $this->db->prepare('SELECT `id` FROM tbl_users ORDER BY id DESC LIMIT 1');
            $stmt->execute();
            $fk_id = $stmt->fetch(PDO::FETCH_ASSOC);
            $this->profile_store($fk_id['id']);
            $this->meal_store($fk_id['id']);
            $_SESSION['success'] = 'You are successfully Registered...';
            header('location: login.php');
        }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
    public function profile_store($fk_id){
        try {
            $sql = "INSERT INTO `profiles` (`user_id`, `first_name`, `last_name`, `mobile`, `alter_mobile`, `occupation`, `facebook_id`, `institute`, `nid`, `room_number`, `address`, `about`, `image`, `created_at`, `updated_at`, `deleted_at`) VALUES ($fk_id, '', '', '01717 000 000', '', '', '', '', '', '00', '', '', '', NOW(), '', '');";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function meal_store($fk_id){
        try {
            $sql_meal = "INSERT INTO `meal` (`user_id`, `meal`, `date`) VALUES ($fk_id, '0', '0-00-2017');";
            $stmt = $this->db->prepare($sql_meal);
            $stmt->execute();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function login(){
        try {
            $sql = "SELECT * FROM `tbl_users` WHERE (name = :name or email = :email) and password = :password";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':name',$this->name,PDO::PARAM_STR);
            $stmt->bindValue(':email',$this->name,PDO::PARAM_STR);
            $stmt->bindValue(':password',$this->password,PDO::PARAM_STR);
            $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


    public function view($id){
        $sql = "SELECT * FROM `profiles` WHERE user_id =:id ";
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue(':id',$id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);

    }
    public function update($id){
        try {
            $sql = "UPDATE `profiles` SET `first_name` = :first_name, `last_name` = :last_name, `mobile` = :mobile, `alter_mobile` = :alter_mobile, `occupation` = :occupation, `facebook_id` = :facebook_id, `institute` = :institute, `nid` = :nid, `room_number` = :room_number, `address` = :address, `about` = :about WHERE `profiles`.`id` = :id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindValue(':first_name', $this->first_name, PDO::PARAM_STR);
            $stmt->bindValue(':last_name', $this->last_name, PDO::PARAM_STR);
            $stmt->bindValue(':mobile', $this->mobile, PDO::PARAM_STR);
            $stmt->bindValue(':alter_mobile', $this->alter_mobile, PDO::PARAM_STR);
            $stmt->bindValue(':occupation', $this->occupation, PDO::PARAM_STR);
            $stmt->bindValue(':facebook_id', $this->facebook_id, PDO::PARAM_STR);
            $stmt->bindValue(':institute', $this->institute, PDO::PARAM_STR);
            $stmt->bindValue(':nid', $this->nid, PDO::PARAM_STR);
            $stmt->bindValue(':room_number', $this->room_number, PDO::PARAM_STR);
            $stmt->bindValue(':address', $this->address, PDO::PARAM_STR);
            $stmt->bindValue(':about', $this->about, PDO::PARAM_STR);
            $stmt->bindValue(':id', $id, PDO::PARAM_STR);
            if($stmt->execute()){
                $_SESSION['success'] = 'Profile Successfully Updated !!!';
                header('location: profile.php');
            }
        }catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /*image uploading and Making a unique name for image*/
      public function image_upload(){
        $_POST['image'] = $_FILES['image']['name'];
        $image_tmp_name = $_FILES['image']['tmp_name'];
        $name =  substr(md5(time()),'0','10');
        $data = explode('.',$_POST['image']);
        $_POST['image'] = $name.'.'.end($data);

        move_uploaded_file($image_tmp_name,'uploads/'.$_POST['image']);
        return $_POST['image'];
    }


}