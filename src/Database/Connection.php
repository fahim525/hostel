<?php

namespace App\Database;
use PDO;
use PDOException;
class Connection
{
    protected $db;
    private $user = 'root';
    private $pass = '';
    public function __construct(){
        try {
            $this->db = new PDO('mysql:host=localhost;dbname=hostel', $this->user, $this->pass);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }


}